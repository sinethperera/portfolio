package Player;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class PlayerView extends JFrame{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private JTextField playerNameField;
    private JButton submitButton;

    private PlayerController controller;

    public PlayerView(PlayerController controller) {
        this.setController(controller);

        playerNameField = new JTextField(20);
        submitButton = new JButton("Submit");

        submitButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                controller.setPlayerName(playerNameField.getText());
            }
        });

        JPanel panel = new JPanel();
        panel.add(new JLabel("Enter Player Name: "));
        panel.add(playerNameField);
        panel.add(submitButton);

        add(panel);
        setTitle("Player Information");
        setSize(300, 150);
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setLocationRelativeTo(null);
    }

    public void display() {
        SwingUtilities.invokeLater(new Runnable() {
            @Override
            public void run() {
                setVisible(true);
            }
        });
    }

	public PlayerController getController() {
		return controller;
	}

	public void setController(PlayerController controller) {
		this.controller = controller;
	}
}
