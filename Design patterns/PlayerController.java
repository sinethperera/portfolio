package Player;

import javax.swing.JFrame;
import javax.swing.JOptionPane;

public class PlayerController {
	private PlayerModel model;
    private PlayerView view;

    public PlayerController(PlayerModel model, PlayerView view) {
        this.model = model;
        this.view = view;
    }

    public void setPlayerName(String playerName) {
        model.setPlayerName(playerName);
        JOptionPane.showMessageDialog(null, "Player Name: " + model.getPlayerName());
    }

    public void initialize() {
        view.display();
    }

    public static void main(String[] args) {
        PlayerModel model = new PlayerModel();
        PlayerView view = new PlayerView(new PlayerController(model, null));
        PlayerController controller = new PlayerController(model, view);

        view.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        controller.initialize();
    }
}
