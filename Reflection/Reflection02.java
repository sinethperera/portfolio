package Reflec;

public class Reflection02 {
	public static void main(String[] args) {
	    Simple s = new Simple();
	    s.squareA();
	    // s.squareB(); // if you uncomment this you will get a compiler error
	    float a = s.a;
	    // float b = s.b; // if you uncomment this you will get a compiler error
	    System.out.println("s=" + s);
	  }
}

