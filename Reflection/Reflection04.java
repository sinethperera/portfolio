package Reflec;

import java.lang.reflect.Field;

public class Reflection04 {
	public static void main(String[] args) throws Exception {
        Simple s = new Simple();
        Field[] fields = s.getClass().getFields();
        System.out.printf("There are %d fields\n", fields.length);
        for (Field f : fields) {
            f.setAccessible(true); // Make the field accessible if it's private
            String fieldType = f.getType().getSimpleName();
            if (fieldType.equals("float")) {
                System.out.printf("field name=%s type=%s value=%.2f\n", f.getName(),
                        fieldType, f.getFloat(s));
            } else {
                System.out.printf("field name=%s type=%s value=%d\n", f.getName(),
                        fieldType, f.getInt(s));
            }
        }
    }
}

